package miage.nuit.info.wtf.common.dao;

import miage.nuit.info.wtf.common.model.Role;
import miage.nuit.info.wtf.common.model.Compte;

public interface UserDAO {

    public Compte getUser(String login);
    
    public Compte createUser(String login, String password, Role role);

}

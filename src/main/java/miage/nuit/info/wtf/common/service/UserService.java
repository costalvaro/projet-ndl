package miage.nuit.info.wtf.common.service;

import miage.nuit.info.wtf.common.model.Compte;

public interface UserService {

    public Compte getUser(String login);

    public Compte createUser(String login, String password);

}

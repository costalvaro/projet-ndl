package miage.nuit.info.wtf.common.dao;

import miage.nuit.info.wtf.common.model.Role;

public interface RoleDAO {

    public Role getRole(int id);
    
    public Role getRole(String roleName);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "entreprise")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entreprise.findAll", query = "SELECT e FROM Entreprise e"),
    @NamedQuery(name = "Entreprise.findByIdEntreprise", query = "SELECT e FROM Entreprise e WHERE e.idEntreprise = :idEntreprise"),
    @NamedQuery(name = "Entreprise.findByRaisonsocialEntreprise", query = "SELECT e FROM Entreprise e WHERE e.raisonsocialEntreprise = :raisonsocialEntreprise"),
    @NamedQuery(name = "Entreprise.findByAdresseEntreprise", query = "SELECT e FROM Entreprise e WHERE e.adresseEntreprise = :adresseEntreprise"),
    @NamedQuery(name = "Entreprise.findByTelEntreprise", query = "SELECT e FROM Entreprise e WHERE e.telEntreprise = :telEntreprise"),
    @NamedQuery(name = "Entreprise.findByMailEntreprise", query = "SELECT e FROM Entreprise e WHERE e.mailEntreprise = :mailEntreprise")})
public class Entreprise implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEntreprise")
    private Integer idEntreprise;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "raison_social_Entreprise")
    private String raisonsocialEntreprise;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "adresse_entreprise")
    private String adresseEntreprise;
    @Size(max = 15)
    @Column(name = "tel_entreprise")
    private String telEntreprise;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "mail_entreprise")
    private String mailEntreprise;
    @OneToMany(mappedBy = "entrepriseidEntreprise")
    private Collection<Projet> projetCollection;
    @OneToMany(mappedBy = "entrepriseidEntreprise")
    private Collection<Compte> compteCollection;

    public Entreprise() {
    }

    public Entreprise(Integer idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public Entreprise(Integer idEntreprise, String raisonsocialEntreprise, String adresseEntreprise, String mailEntreprise) {
        this.idEntreprise = idEntreprise;
        this.raisonsocialEntreprise = raisonsocialEntreprise;
        this.adresseEntreprise = adresseEntreprise;
        this.mailEntreprise = mailEntreprise;
    }

    public Integer getIdEntreprise() {
        return idEntreprise;
    }

    public void setIdEntreprise(Integer idEntreprise) {
        this.idEntreprise = idEntreprise;
    }

    public String getRaisonsocialEntreprise() {
        return raisonsocialEntreprise;
    }

    public void setRaisonsocialEntreprise(String raisonsocialEntreprise) {
        this.raisonsocialEntreprise = raisonsocialEntreprise;
    }

    public String getAdresseEntreprise() {
        return adresseEntreprise;
    }

    public void setAdresseEntreprise(String adresseEntreprise) {
        this.adresseEntreprise = adresseEntreprise;
    }

    public String getTelEntreprise() {
        return telEntreprise;
    }

    public void setTelEntreprise(String telEntreprise) {
        this.telEntreprise = telEntreprise;
    }

    public String getMailEntreprise() {
        return mailEntreprise;
    }

    public void setMailEntreprise(String mailEntreprise) {
        this.mailEntreprise = mailEntreprise;
    }

    @XmlTransient
    public Collection<Projet> getProjetCollection() {
        return projetCollection;
    }

    public void setProjetCollection(Collection<Projet> projetCollection) {
        this.projetCollection = projetCollection;
    }

    @XmlTransient
    public Collection<Compte> getCompteCollection() {
        return compteCollection;
    }

    public void setCompteCollection(Collection<Compte> compteCollection) {
        this.compteCollection = compteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEntreprise != null ? idEntreprise.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entreprise)) {
            return false;
        }
        Entreprise other = (Entreprise) object;
        if ((this.idEntreprise == null && other.idEntreprise != null) || (this.idEntreprise != null && !this.idEntreprise.equals(other.idEntreprise))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.Entreprise[ idEntreprise=" + idEntreprise + " ]";
    }
    
}

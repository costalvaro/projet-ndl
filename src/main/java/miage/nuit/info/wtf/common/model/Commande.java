/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "commande")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c"),
    @NamedQuery(name = "Commande.findByPanieridPanier", query = "SELECT c FROM Commande c WHERE c.commandePK.panieridPanier = :panieridPanier"),
    @NamedQuery(name = "Commande.findByProduitidProduit", query = "SELECT c FROM Commande c WHERE c.commandePK.produitidProduit = :produitidProduit"),
    @NamedQuery(name = "Commande.findByQuantiteProduit", query = "SELECT c FROM Commande c WHERE c.quantiteProduit = :quantiteProduit")})
public class Commande implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CommandePK commandePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantite_produit")
    private int quantiteProduit;
    @JoinColumn(name = "Produit_idProduit", referencedColumnName = "idProduit", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produit produit;
    @JoinColumn(name = "Panier_idPanier", referencedColumnName = "idPanier", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Panier panier;

    public Commande() {
    }

    public Commande(CommandePK commandePK) {
        this.commandePK = commandePK;
    }

    public Commande(CommandePK commandePK, int quantiteProduit) {
        this.commandePK = commandePK;
        this.quantiteProduit = quantiteProduit;
    }

    public Commande(int panieridPanier, int produitidProduit) {
        this.commandePK = new CommandePK(panieridPanier, produitidProduit);
    }

    public CommandePK getCommandePK() {
        return commandePK;
    }

    public void setCommandePK(CommandePK commandePK) {
        this.commandePK = commandePK;
    }

    public int getQuantiteProduit() {
        return quantiteProduit;
    }

    public void setQuantiteProduit(int quantiteProduit) {
        this.quantiteProduit = quantiteProduit;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Panier getPanier() {
        return panier;
    }

    public void setPanier(Panier panier) {
        this.panier = panier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commandePK != null ? commandePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.commandePK == null && other.commandePK != null) || (this.commandePK != null && !this.commandePK.equals(other.commandePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.Commande[ commandePK=" + commandePK + " ]";
    }
    
}

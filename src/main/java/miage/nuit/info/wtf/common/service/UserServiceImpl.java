package miage.nuit.info.wtf.common.service;

import miage.nuit.info.wtf.common.dao.RoleDAO;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import miage.nuit.info.wtf.common.dao.UserDAO;
import miage.nuit.info.wtf.common.model.Role;
import miage.nuit.info.wtf.common.model.Compte;
import org.apache.log4j.Logger;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    public Compte getUser(String login) {
        return userDAO.getUser(login);
    }

    public Compte createUser(String login, String password) {
        logger.info("Création de l'utilisateur " + login);
        // Inscription en tant qu'utilisateur standard
        Role role = roleDAO.getRole("user");
        // Enregistrement du tout
        Compte user = userDAO.createUser(login, password, role);
        return user;
    }

}

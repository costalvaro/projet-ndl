package miage.nuit.info.wtf.common.service;

import miage.nuit.info.wtf.common.model.Role;

public interface RoleService {
	
	public Role getRole(int id);

}

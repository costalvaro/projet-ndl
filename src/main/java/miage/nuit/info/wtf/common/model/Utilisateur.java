/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "utilisateur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilisateur.findAll", query = "SELECT u FROM Utilisateur u"),
    @NamedQuery(name = "Utilisateur.findByIdUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.idUtilisateur = :idUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByNomUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.nomUtilisateur = :nomUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByPrenomUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.prenomUtilisateur = :prenomUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByAdresseUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.adresseUtilisateur = :adresseUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByTelUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.telUtilisateur = :telUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByMailUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.mailUtilisateur = :mailUtilisateur"),
    @NamedQuery(name = "Utilisateur.findByAnneNaissanceUtilisateur", query = "SELECT u FROM Utilisateur u WHERE u.anneNaissanceUtilisateur = :anneNaissanceUtilisateur")})
public class Utilisateur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idUtilisateur")
    private Integer idUtilisateur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nom_utilisateur")
    private String nomUtilisateur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "prenom_utilisateur")
    private String prenomUtilisateur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "adresse_utilisateur")
    private String adresseUtilisateur;
    @Size(max = 45)
    @Column(name = "tel_utilisateur")
    private String telUtilisateur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "mail_utilisateur")
    private String mailUtilisateur;
    @Column(name = "anne_naissance_utilisateur")
    @Temporal(TemporalType.DATE)
    private Date anneNaissanceUtilisateur;
    @JoinTable(name = "centre_interet_utilisateur", joinColumns = {
        @JoinColumn(name = "Utilisateur_idUtilisateur", referencedColumnName = "idUtilisateur")}, inverseJoinColumns = {
        @JoinColumn(name = "centre_interet_idcentre_interet", referencedColumnName = "idcentre_interet")})
    @ManyToMany
    private Collection<CentreInteret> centreInteretCollection;
    @JoinTable(name = "utilisateur_has_projet", joinColumns = {
        @JoinColumn(name = "Utilisateur_idUtilisateur", referencedColumnName = "idUtilisateur")}, inverseJoinColumns = {
        @JoinColumn(name = "Projet_idProjet", referencedColumnName = "idProjet")})
    @ManyToMany
    private Collection<Projet> projetCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utilisateuridUtilisateur")
    private Collection<Projet> projetCollection1;
    @OneToMany(mappedBy = "utilisateuridUtilisateur")
    private Collection<Compte> compteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "utilisateuridUtilisateur")
    private Collection<Panier> panierCollection;

    public Utilisateur() {
    }

    public Utilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public Utilisateur(Integer idUtilisateur, String nomUtilisateur, String prenomUtilisateur, String adresseUtilisateur, String mailUtilisateur) {
        this.idUtilisateur = idUtilisateur;
        this.nomUtilisateur = nomUtilisateur;
        this.prenomUtilisateur = prenomUtilisateur;
        this.adresseUtilisateur = adresseUtilisateur;
        this.mailUtilisateur = mailUtilisateur;
    }

    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    public void setNomUtilisateur(String nomUtilisateur) {
        this.nomUtilisateur = nomUtilisateur;
    }

    public String getPrenomUtilisateur() {
        return prenomUtilisateur;
    }

    public void setPrenomUtilisateur(String prenomUtilisateur) {
        this.prenomUtilisateur = prenomUtilisateur;
    }

    public String getAdresseUtilisateur() {
        return adresseUtilisateur;
    }

    public void setAdresseUtilisateur(String adresseUtilisateur) {
        this.adresseUtilisateur = adresseUtilisateur;
    }

    public String getTelUtilisateur() {
        return telUtilisateur;
    }

    public void setTelUtilisateur(String telUtilisateur) {
        this.telUtilisateur = telUtilisateur;
    }

    public String getMailUtilisateur() {
        return mailUtilisateur;
    }

    public void setMailUtilisateur(String mailUtilisateur) {
        this.mailUtilisateur = mailUtilisateur;
    }

    public Date getAnneNaissanceUtilisateur() {
        return anneNaissanceUtilisateur;
    }

    public void setAnneNaissanceUtilisateur(Date anneNaissanceUtilisateur) {
        this.anneNaissanceUtilisateur = anneNaissanceUtilisateur;
    }

    @XmlTransient
    public Collection<CentreInteret> getCentreInteretCollection() {
        return centreInteretCollection;
    }

    public void setCentreInteretCollection(Collection<CentreInteret> centreInteretCollection) {
        this.centreInteretCollection = centreInteretCollection;
    }

    @XmlTransient
    public Collection<Projet> getProjetCollection() {
        return projetCollection;
    }

    public void setProjetCollection(Collection<Projet> projetCollection) {
        this.projetCollection = projetCollection;
    }

    @XmlTransient
    public Collection<Projet> getProjetCollection1() {
        return projetCollection1;
    }

    public void setProjetCollection1(Collection<Projet> projetCollection1) {
        this.projetCollection1 = projetCollection1;
    }

    @XmlTransient
    public Collection<Compte> getCompteCollection() {
        return compteCollection;
    }

    public void setCompteCollection(Collection<Compte> compteCollection) {
        this.compteCollection = compteCollection;
    }

    @XmlTransient
    public Collection<Panier> getPanierCollection() {
        return panierCollection;
    }

    public void setPanierCollection(Collection<Panier> panierCollection) {
        this.panierCollection = panierCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUtilisateur != null ? idUtilisateur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilisateur)) {
            return false;
        }
        Utilisateur other = (Utilisateur) object;
        if ((this.idUtilisateur == null && other.idUtilisateur != null) || (this.idUtilisateur != null && !this.idUtilisateur.equals(other.idUtilisateur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.Utilisateur[ idUtilisateur=" + idUtilisateur + " ]";
    }
    
}

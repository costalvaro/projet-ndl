package miage.nuit.info.wtf.common.dao;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import miage.nuit.info.wtf.common.model.Role;
import org.hibernate.Query;

@Repository
public class RoleDAOImpl implements RoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public Role getRole(int id) {
        Role role = (Role) getCurrentSession().load(Role.class, id);
        return role;
    }

    public Role getRole(String roleName) {
        Role role = null;
        List<Role> roleList = new ArrayList<Role>();
        Query query = getCurrentSession().createQuery("from Role r where r.lebelleRole = :rolename");
        query.setParameter("rolename", roleName);
        roleList = query.list();
        if (roleList.size() > 0) {
            return roleList.get(0);
        } else {
            return null;
        }
    }
}

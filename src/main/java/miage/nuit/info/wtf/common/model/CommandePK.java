/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alc
 */
@Embeddable
public class CommandePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "Panier_idPanier")
    private int panieridPanier;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Produit_idProduit")
    private int produitidProduit;

    public CommandePK() {
    }

    public CommandePK(int panieridPanier, int produitidProduit) {
        this.panieridPanier = panieridPanier;
        this.produitidProduit = produitidProduit;
    }

    public int getPanieridPanier() {
        return panieridPanier;
    }

    public void setPanieridPanier(int panieridPanier) {
        this.panieridPanier = panieridPanier;
    }

    public int getProduitidProduit() {
        return produitidProduit;
    }

    public void setProduitidProduit(int produitidProduit) {
        this.produitidProduit = produitidProduit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) panieridPanier;
        hash += (int) produitidProduit;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommandePK)) {
            return false;
        }
        CommandePK other = (CommandePK) object;
        if (this.panieridPanier != other.panieridPanier) {
            return false;
        }
        if (this.produitidProduit != other.produitidProduit) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.CommandePK[ panieridPanier=" + panieridPanier + ", produitidProduit=" + produitidProduit + " ]";
    }
    
}

package miage.nuit.info.wtf.common.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import miage.nuit.info.wtf.common.dao.UserDAO;
import miage.nuit.info.wtf.common.model.Role;
import org.apache.log4j.Logger;

@Service
@Transactional(readOnly = true)
public class CustomUserDetailsService implements UserDetailsService {
    
    private Logger logger = Logger.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserDAO userDAO;

    public UserDetails loadUserByUsername(String login)
            throws UsernameNotFoundException {
        logger.info("On récupère les droits du user");
        miage.nuit.info.wtf.common.model.Compte domainUser = userDAO.getUser(login);
        logger.info("User récupéré:" + System.getProperty("line.separator")
        + domainUser.getLoginCompte() + " (role " + domainUser.getRoleIdrole().getLebelleRole() + ")");
        
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new User(
                domainUser.getLoginCompte(),
                domainUser.getMdpCompte(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getAuthorities(domainUser.getRoleIdrole())
        );
    }

    public Collection<? extends GrantedAuthority> getAuthorities(Role role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getAccessRoles(role));
        return authList;
    }

    public List<String> getAccessRoles(Role role) {

        List<String> roles = new ArrayList<String>();

        if ("user".equals(role.getLebelleRole())) {
            roles.add("ROLE_USER");
        } else if ("moderator".equals(role.getLebelleRole())) {
            roles.add("ROLE_USER");
            roles.add("ROLE_MODERATOR");
        } else if ("admin".equals(role.getLebelleRole())) {
            roles.add("ROLE_USER");
            roles.add("ROLE_MODERATOR");
            roles.add("ROLE_ADMIN");
        }
        String rolesDisplay = "User logged in with the following roles:" + System.getProperty("line.separator");
        for (String r : roles)
            rolesDisplay += r + System.getProperty("line.separator");
        System.out.println(rolesDisplay);
        
        return roles;
    }

    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

}

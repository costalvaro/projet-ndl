/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "compte")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compte.findAll", query = "SELECT c FROM Compte c"),
    @NamedQuery(name = "Compte.findByIdcompte", query = "SELECT c FROM Compte c WHERE c.idcompte = :idcompte"),
    @NamedQuery(name = "Compte.findByLoginCompte", query = "SELECT c FROM Compte c WHERE c.loginCompte = :loginCompte"),
    @NamedQuery(name = "Compte.findByMdpCompte", query = "SELECT c FROM Compte c WHERE c.mdpCompte = :mdpCompte")})
public class Compte implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcompte")
    private Integer idcompte;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "login_compte")
    private String loginCompte;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "mdp_compte")
    private String mdpCompte;
    @JoinColumn(name = "role_idrole", referencedColumnName = "idrole")
    @ManyToOne(optional = false)
    private Role roleIdrole;
    @JoinColumn(name = "Entreprise_idEntreprise", referencedColumnName = "idEntreprise")
    @ManyToOne
    private Entreprise entrepriseidEntreprise;
    @JoinColumn(name = "Utilisateur_idUtilisateur", referencedColumnName = "idUtilisateur")
    @ManyToOne
    private Utilisateur utilisateuridUtilisateur;

    public Compte() {
    }

    public Compte(Integer idcompte) {
        this.idcompte = idcompte;
    }

    public Compte(Integer idcompte, String loginCompte, String mdpCompte) {
        this.idcompte = idcompte;
        this.loginCompte = loginCompte;
        this.mdpCompte = mdpCompte;
    }

    public Integer getIdcompte() {
        return idcompte;
    }

    public void setIdcompte(Integer idcompte) {
        this.idcompte = idcompte;
    }

    public String getLoginCompte() {
        return loginCompte;
    }

    public void setLoginCompte(String loginCompte) {
        this.loginCompte = loginCompte;
    }

    public String getMdpCompte() {
        return mdpCompte;
    }

    public void setMdpCompte(String mdpCompte) {
        this.mdpCompte = mdpCompte;
    }

    public Role getRoleIdrole() {
        return roleIdrole;
    }

    public void setRoleIdrole(Role roleIdrole) {
        this.roleIdrole = roleIdrole;
    }

    public Entreprise getEntrepriseidEntreprise() {
        return entrepriseidEntreprise;
    }

    public void setEntrepriseidEntreprise(Entreprise entrepriseidEntreprise) {
        this.entrepriseidEntreprise = entrepriseidEntreprise;
    }

    public Utilisateur getUtilisateuridUtilisateur() {
        return utilisateuridUtilisateur;
    }

    public void setUtilisateuridUtilisateur(Utilisateur utilisateuridUtilisateur) {
        this.utilisateuridUtilisateur = utilisateuridUtilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcompte != null ? idcompte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compte)) {
            return false;
        }
        Compte other = (Compte) object;
        if ((this.idcompte == null && other.idcompte != null) || (this.idcompte != null && !this.idcompte.equals(other.idcompte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.Compte[ idcompte=" + idcompte + " ]";
    }
    
}

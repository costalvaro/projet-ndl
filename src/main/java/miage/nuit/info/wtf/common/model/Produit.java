/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "produit")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produit.findAll", query = "SELECT p FROM Produit p"),
    @NamedQuery(name = "Produit.findByIdProduit", query = "SELECT p FROM Produit p WHERE p.idProduit = :idProduit"),
    @NamedQuery(name = "Produit.findByNomProduit", query = "SELECT p FROM Produit p WHERE p.nomProduit = :nomProduit"),
    @NamedQuery(name = "Produit.findByPrixProduit", query = "SELECT p FROM Produit p WHERE p.prixProduit = :prixProduit")})
public class Produit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProduit")
    private Integer idProduit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nom_Produit")
    private String nomProduit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prix_produit")
    private float prixProduit;
    @JoinTable(name = "centre_interet_produit", joinColumns = {
        @JoinColumn(name = "Produit_idProduit", referencedColumnName = "idProduit")}, inverseJoinColumns = {
        @JoinColumn(name = "centre_interet_idcentre_interet", referencedColumnName = "idcentre_interet")})
    @ManyToMany
    private Collection<CentreInteret> centreInteretCollection;
    @JoinColumn(name = "Marque_idMarque", referencedColumnName = "idMarque")
    @ManyToOne(optional = false)
    private Marque marqueidMarque;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "produit")
    private Collection<Commande> commandeCollection;

    public Produit() {
    }

    public Produit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public Produit(Integer idProduit, String nomProduit, float prixProduit) {
        this.idProduit = idProduit;
        this.nomProduit = nomProduit;
        this.prixProduit = prixProduit;
    }

    public Integer getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Integer idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public float getPrixProduit() {
        return prixProduit;
    }

    public void setPrixProduit(float prixProduit) {
        this.prixProduit = prixProduit;
    }

    @XmlTransient
    public Collection<CentreInteret> getCentreInteretCollection() {
        return centreInteretCollection;
    }

    public void setCentreInteretCollection(Collection<CentreInteret> centreInteretCollection) {
        this.centreInteretCollection = centreInteretCollection;
    }

    public Marque getMarqueidMarque() {
        return marqueidMarque;
    }

    public void setMarqueidMarque(Marque marqueidMarque) {
        this.marqueidMarque = marqueidMarque;
    }

    @XmlTransient
    public Collection<Commande> getCommandeCollection() {
        return commandeCollection;
    }

    public void setCommandeCollection(Collection<Commande> commandeCollection) {
        this.commandeCollection = commandeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduit != null ? idProduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.idProduit == null && other.idProduit != null) || (this.idProduit != null && !this.idProduit.equals(other.idProduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.Produit[ idProduit=" + idProduit + " ]";
    }
    
}

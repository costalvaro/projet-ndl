/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package miage.nuit.info.wtf.common.model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author alc
 */
@Entity
@Table(name = "centre_interet")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CentreInteret.findAll", query = "SELECT c FROM CentreInteret c"),
    @NamedQuery(name = "CentreInteret.findByIdcentreInteret", query = "SELECT c FROM CentreInteret c WHERE c.idcentreInteret = :idcentreInteret"),
    @NamedQuery(name = "CentreInteret.findByTitreCentreInteret", query = "SELECT c FROM CentreInteret c WHERE c.titreCentreInteret = :titreCentreInteret"),
    @NamedQuery(name = "CentreInteret.findByDescriptionCentreInteret", query = "SELECT c FROM CentreInteret c WHERE c.descriptionCentreInteret = :descriptionCentreInteret")})
public class CentreInteret implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcentre_interet")
    private Integer idcentreInteret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "titre_centre_interet")
    private String titreCentreInteret;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "description_centre_interet")
    private String descriptionCentreInteret;
    @ManyToMany(mappedBy = "centreInteretCollection")
    private Collection<Projet> projetCollection;
    @ManyToMany(mappedBy = "centreInteretCollection")
    private Collection<Produit> produitCollection;
    @ManyToMany(mappedBy = "centreInteretCollection")
    private Collection<Utilisateur> utilisateurCollection;

    public CentreInteret() {
    }

    public CentreInteret(Integer idcentreInteret) {
        this.idcentreInteret = idcentreInteret;
    }

    public CentreInteret(Integer idcentreInteret, String titreCentreInteret, String descriptionCentreInteret) {
        this.idcentreInteret = idcentreInteret;
        this.titreCentreInteret = titreCentreInteret;
        this.descriptionCentreInteret = descriptionCentreInteret;
    }

    public Integer getIdcentreInteret() {
        return idcentreInteret;
    }

    public void setIdcentreInteret(Integer idcentreInteret) {
        this.idcentreInteret = idcentreInteret;
    }

    public String getTitreCentreInteret() {
        return titreCentreInteret;
    }

    public void setTitreCentreInteret(String titreCentreInteret) {
        this.titreCentreInteret = titreCentreInteret;
    }

    public String getDescriptionCentreInteret() {
        return descriptionCentreInteret;
    }

    public void setDescriptionCentreInteret(String descriptionCentreInteret) {
        this.descriptionCentreInteret = descriptionCentreInteret;
    }

    @XmlTransient
    public Collection<Projet> getProjetCollection() {
        return projetCollection;
    }

    public void setProjetCollection(Collection<Projet> projetCollection) {
        this.projetCollection = projetCollection;
    }

    @XmlTransient
    public Collection<Produit> getProduitCollection() {
        return produitCollection;
    }

    public void setProduitCollection(Collection<Produit> produitCollection) {
        this.produitCollection = produitCollection;
    }

    @XmlTransient
    public Collection<Utilisateur> getUtilisateurCollection() {
        return utilisateurCollection;
    }

    public void setUtilisateurCollection(Collection<Utilisateur> utilisateurCollection) {
        this.utilisateurCollection = utilisateurCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcentreInteret != null ? idcentreInteret.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentreInteret)) {
            return false;
        }
        CentreInteret other = (CentreInteret) object;
        if ((this.idcentreInteret == null && other.idcentreInteret != null) || (this.idcentreInteret != null && !this.idcentreInteret.equals(other.idcentreInteret))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "miage.nuit.info.wtf.common.model.CentreInteret[ idcentreInteret=" + idcentreInteret + " ]";
    }
    
}

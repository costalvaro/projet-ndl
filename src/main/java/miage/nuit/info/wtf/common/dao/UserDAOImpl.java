package miage.nuit.info.wtf.common.dao;

import java.util.ArrayList;
import java.util.List;
import miage.nuit.info.wtf.common.model.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import miage.nuit.info.wtf.common.model.Compte;
import org.apache.log4j.Logger;

@Repository
public class UserDAOImpl implements UserDAO {
    
    private Logger logger = Logger.getLogger(UserDAOImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    private Session openSession() {
        return sessionFactory.getCurrentSession();
    }

    public Compte getUser(String login) {
        logger.info("Récupération du user de login " + login);
        logger.info(sessionFactory);
        List<Compte> userList = new ArrayList<Compte>();
        Query query = openSession().createQuery("from Compte c where c.loginCompte = :login");
        query.setParameter("login", login);
        userList = query.list();
        if (userList.size() > 0) {
            Compte tmp = userList.get(0);
            System.out.println("User: " + tmp.getLoginCompte() + " / " + tmp.getMdpCompte());
            return userList.get(0);
        } else {
            return null;
        }
    }
    
    public Compte createUser(String login, String password, Role role) {
        Compte newUser = new Compte();
        newUser.setLoginCompte(login);
        newUser.setMdpCompte(password);
        newUser.setRoleIdrole(role);
        logger.info("Enregistrement de l'utilisateur " + newUser.getLoginCompte()+ "/" + newUser.getMdpCompte()+ " (" + newUser.getRoleIdrole().getLebelleRole() + ")");
        openSession().save(newUser);
        
        return newUser;
    }
}

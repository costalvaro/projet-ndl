package miage.nuit.info.wtf.auth.beans;

import javax.validation.constraints.Size;
import miage.nuit.info.wtf.controller.util.annotations.FieldMatch;
import org.hibernate.validator.constraints.NotEmpty;

@FieldMatch.List({
    @FieldMatch(first = "password", second = "confirmPassword", message = "Les deux mots de passe doivent être identiques"),})
public class UserBean {

    @NotEmpty()
    @Size(min = 4, max = 20)
    private String login;

    @NotEmpty()
    @Size(min = 4, max = 20)
    private String password;

    @NotEmpty()
    @Size(min = 4, max = 20)
    private String confirmPassword;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}

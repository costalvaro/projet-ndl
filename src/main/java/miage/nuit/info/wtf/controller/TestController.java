package miage.nuit.info.wtf.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Secured("ROLE_ADMIN")
public class TestController {

    private static final Logger logger = Logger.getLogger(TestController.class);
    
    @Secured("ROLE_ADMIN")
    @RequestMapping("/test")
	protected String printTest(ModelMap model) {
		model.addAttribute("message", "Voilà mon message !");
	
		//log it
		if(logger.isDebugEnabled()){
			logger.debug(model);
		}
		
		return "TestPage";
	}
        
        @Secured("ROLE_ADMIN")
        @RequestMapping(value="/test/{name:.*}", method=RequestMethod.GET)
	protected String printTest2(@PathVariable String name, ModelMap model) {
		model.addAttribute("message", "On a la val " + name);
	
		//log it
		if(logger.isDebugEnabled()){
			logger.debug(model);
		}
		
		return "TestPage";
	}
        
}

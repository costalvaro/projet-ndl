package miage.nuit.info.wtf.controller;

import javax.validation.Valid;
import miage.nuit.info.wtf.auth.beans.UserBean;
import miage.nuit.info.wtf.common.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

@Controller
public class AuthController {

    private static final Logger logger = Logger.getLogger(AuthController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    protected String loginForm(ModelMap model) {
        return "auth/LoginForm";
    }

    @RequestMapping(value = "/login-error", method = RequestMethod.GET)
    protected String invalidLogin(ModelMap model) {
        model.addAttribute("error", true);
        return "auth/LoginForm";
    }

    @RequestMapping(value = "/login-success", method = RequestMethod.GET)
    protected String successLogin(ModelMap model) {
        return "auth/LoginSuccess";
    }

    @RequestMapping(value = "/inscription", method = RequestMethod.GET)
    protected String inscription(ModelMap model) {
        UserBean user = new UserBean();
        model.addAttribute("user", user);

        return "auth/InscriptionForm";
    }

    @RequestMapping(value = "/inscription", method = RequestMethod.POST)
    protected String validationInscription(@ModelAttribute("user") @Valid UserBean user, BindingResult result, SessionStatus status) {
        // @Valid devant notre bean le vérifie et donne les résultats de vérification dans result
        // On vérifie result pour savoir si l'on a des erreurs de saisie
        if (result.hasErrors()) {
            // On redirige sur le formulaire en cas d'erreur
            return "auth/InscriptionForm";
        } else {
            // On effectue la création de l'utilisateur si le formulaire est correct 
            status.setComplete();
            // Cryptage du mot de passe renseigné
            StandardPasswordEncoder encoder = new StandardPasswordEncoder();
            String encodedPassword = encoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            user.setConfirmPassword(encodedPassword);
            // Inscription en base de la personne
            logger.info("Validation du formulaire d'inscription...");
            userService.createUser(user.getLogin(), user.getPassword());
            logger.info("Validation du formulaire d'inscription terminée...");
            return "auth/InscriptionSuccess";
        }
    }
}

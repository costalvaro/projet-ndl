package miage.nuit.info.wtf.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WelcomeController {

    private static final Logger logger = Logger.getLogger(WelcomeController.class);
    
    @RequestMapping(value={"/","/welcome"}, method = RequestMethod.GET)
	protected String welcomePage(ModelMap model) throws Exception {
		model.addAttribute("msg", "Page d'acceuil de test");
	
		if(logger.isDebugEnabled()){
			logger.debug(model);
		}
		
		return "WelcomePage";
	}

}

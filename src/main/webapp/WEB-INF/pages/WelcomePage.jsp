<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <body>
        <h1>Spring MVC internationalization example</h1>

        Language : <a href="?language=fr">Français</a>|
        <a href="?language=en_EN">Anglais</a>

        <h3>
            welcome.springmvc : <spring:message code="welcome.springmvc" text="default text" />
        </h3>
        <h2>${msg}</h2>


        Current Locale : ${pageContext.response.locale}

        <sec:authorize access="isAnonymous()">
            <form method="POST" action="<c:url value='j_spring_security_check'/>">
                Username: <input name="j_username" type="text" value="${SPRING_SECURITY_LAST_USERNAME}" /> 
                Password: <input name="j_password" type="password" /> 
                <input type="submit" value="Sign in" />
            </form>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            Logged as <sec:authentication property="principal.username" />
            <a href="<c:url value="/j_spring_security_logout" />">Logout</a>
        </sec:authorize>

    </body>
</html>
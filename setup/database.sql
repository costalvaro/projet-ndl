-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 06 Décembre 2013 à 04:14
-- Version du serveur: 5.5.29
-- Version de PHP: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données: `testbdd`
--

-- --------------------------------------------------------

--
-- Structure de la table `centre_interet`
--

CREATE TABLE `centre_interet` (
  `idcentre_interet` int(11) NOT NULL AUTO_INCREMENT,
  `titre_centre_interet` varchar(45) NOT NULL,
  `description_centre_interet` varchar(80) NOT NULL,
  PRIMARY KEY (`idcentre_interet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `centre_interet_produit`
--

CREATE TABLE `centre_interet_produit` (
  `Produit_idProduit` int(11) NOT NULL,
  `centre_interet_idcentre_interet` int(11) NOT NULL,
  PRIMARY KEY (`Produit_idProduit`,`centre_interet_idcentre_interet`),
  KEY `fk_Produit_has_centre_interet_centre_interet_idx` (`centre_interet_idcentre_interet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `centre_interet_projet`
--

CREATE TABLE `centre_interet_projet` (
  `Projet_idProjet` int(11) NOT NULL,
  `centre_interet_idcentre_interet` int(11) NOT NULL,
  PRIMARY KEY (`Projet_idProjet`,`centre_interet_idcentre_interet`),
  KEY `fk_Produit_has_centre_interet_centre_interet_idx` (`centre_interet_idcentre_interet`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `centre_interet_utilisateur`
--

CREATE TABLE `centre_interet_utilisateur` (
  `Utilisateur_idUtilisateur` int(11) NOT NULL,
  `centre_interet_idcentre_interet` int(11) NOT NULL,
  PRIMARY KEY (`Utilisateur_idUtilisateur`,`centre_interet_idcentre_interet`),
  KEY `fk_Utilisateur_has_centre_interet_centre_interet1_idx` (`centre_interet_idcentre_interet`),
  KEY `fk_Utilisateur_has_centre_interet_Utilisateur1_idx` (`Utilisateur_idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `Panier_idPanier` int(11) NOT NULL,
  `Produit_idProduit` int(11) NOT NULL,
  `quantite_produit` int(11) NOT NULL,
  PRIMARY KEY (`Panier_idPanier`,`Produit_idProduit`),
  KEY `fk_Panier_has_Produit_Produit1_idx` (`Produit_idProduit`),
  KEY `fk_Panier_has_Produit_Panier1_idx` (`Panier_idPanier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE `compte` (
  `idcompte` int(11) NOT NULL AUTO_INCREMENT,
  `login_compte` varchar(50) NOT NULL,
  `mdp_compte` varchar(150) NOT NULL,
  `Utilisateur_idUtilisateur` int(11) DEFAULT NULL,
  `Entreprise_idEntreprise` int(11) DEFAULT NULL,
  `role_idrole` int(11) NOT NULL,
  PRIMARY KEY (`idcompte`),
  UNIQUE KEY `login_compte_UNIQUE` (`login_compte`),
  KEY `fk_compte_Utilisateur1_idx` (`Utilisateur_idUtilisateur`),
  KEY `fk_compte_Entreprise1_idx` (`Entreprise_idEntreprise`),
  KEY `fk_compte_role1_idx` (`role_idrole`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`idcompte`, `login_compte`, `mdp_compte`, `Utilisateur_idUtilisateur`, `Entreprise_idEntreprise`, `role_idrole`) VALUES
(1, 'admin', '10619872970d250ef144e1cc30a03b6c8202e687a1206d10213bec489f0f5d6a79db69df5d8f4a17', NULL, NULL, 4),
(2, 'user', 'e66d3223666b2b5df97869723bc5878ba763aee7a54966b3691ee92553368ee8950113455ba02f4d', NULL, NULL, 1),
(3, 'modo', 'e64acb6e8bc9ab7498452bfc42ee9ea29d2096af8f501a720cf8c479442112bc15d92857e77a5220', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `idEntreprise` int(11) NOT NULL AUTO_INCREMENT,
  `raison_social_Entreprise` varchar(45) NOT NULL,
  `adresse_entreprise` varchar(80) NOT NULL,
  `tel_entreprise` varchar(15) DEFAULT NULL,
  `mail_entreprise` varchar(45) NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `marque`
--

CREATE TABLE `marque` (
  `idMarque` int(11) NOT NULL AUTO_INCREMENT,
  `nom_marque` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idMarque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE `panier` (
  `idPanier` int(11) NOT NULL AUTO_INCREMENT,
  `statut_Panier` varchar(30) NOT NULL COMMENT 'statut = ENUM(''en cours'', ''payé'')',
  `Utilisateur_idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY (`idPanier`),
  KEY `fk_Panier_Utilisateur1_idx` (`Utilisateur_idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Produit` varchar(45) NOT NULL,
  `prix_produit` float NOT NULL,
  `Marque_idMarque` int(11) NOT NULL,
  PRIMARY KEY (`idProduit`),
  KEY `fk_Produit_Marque_idx` (`Marque_idMarque`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE `projet` (
  `idProjet` int(11) NOT NULL AUTO_INCREMENT,
  `nom_projet` varchar(45) NOT NULL,
  `Entreprise_idEntreprise` int(11) DEFAULT NULL,
  `Utilisateur_idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY (`idProjet`),
  KEY `fk_Projet_Entreprise1_idx` (`Entreprise_idEntreprise`),
  KEY `fk_Projet_Utilisateur1_idx` (`Utilisateur_idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `lebelle_role` varchar(45) NOT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `role`
--

INSERT INTO `role` (`idrole`, `lebelle_role`) VALUES
(1, 'user'),
(2, 'moderateur'),
(3, 'entreprise'),
(4, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` varchar(45) NOT NULL,
  `prenom_utilisateur` varchar(45) NOT NULL,
  `adresse_utilisateur` varchar(60) NOT NULL,
  `tel_utilisateur` varchar(45) DEFAULT NULL,
  `mail_utilisateur` varchar(45) NOT NULL,
  `anne_naissance_utilisateur` date DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_has_projet`
--

CREATE TABLE `utilisateur_has_projet` (
  `Utilisateur_idUtilisateur` int(11) NOT NULL,
  `Projet_idProjet` int(11) NOT NULL,
  PRIMARY KEY (`Utilisateur_idUtilisateur`,`Projet_idProjet`),
  KEY `fk_Utilisateur_has_Projet_Projet1_idx` (`Projet_idProjet`),
  KEY `fk_Utilisateur_has_Projet_Utilisateur1_idx` (`Utilisateur_idUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `centre_interet_produit`
--
ALTER TABLE `centre_interet_produit`
  ADD CONSTRAINT `fk_Produit_has_centre_interet_Utilisateur` FOREIGN KEY (`Produit_idProduit`) REFERENCES `produit` (`idProduit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Produit_has_centre_interet_centre_interet` FOREIGN KEY (`centre_interet_idcentre_interet`) REFERENCES `centre_interet` (`idcentre_interet`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `centre_interet_projet`
--
ALTER TABLE `centre_interet_projet`
  ADD CONSTRAINT `fk_Produit_has_centre_interet_Projet` FOREIGN KEY (`Projet_idProjet`) REFERENCES `projet` (`idProjet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Produit_has_centre_interet_centre_interet0` FOREIGN KEY (`centre_interet_idcentre_interet`) REFERENCES `centre_interet` (`idcentre_interet`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `centre_interet_utilisateur`
--
ALTER TABLE `centre_interet_utilisateur`
  ADD CONSTRAINT `fk_Utilisateur_has_centre_interet_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Utilisateur_has_centre_interet_centre_interet1` FOREIGN KEY (`centre_interet_idcentre_interet`) REFERENCES `centre_interet` (`idcentre_interet`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_Panier_has_Produit_Panier1` FOREIGN KEY (`Panier_idPanier`) REFERENCES `panier` (`idPanier`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Panier_has_Produit_Produit1` FOREIGN KEY (`Produit_idProduit`) REFERENCES `produit` (`idProduit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `fk_compte_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compte_Entreprise1` FOREIGN KEY (`Entreprise_idEntreprise`) REFERENCES `entreprise` (`idEntreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_compte_role1` FOREIGN KEY (`role_idrole`) REFERENCES `role` (`idrole`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `fk_Panier_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_Produit_Marque` FOREIGN KEY (`Marque_idMarque`) REFERENCES `marque` (`idMarque`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `fk_Projet_Entreprise1` FOREIGN KEY (`Entreprise_idEntreprise`) REFERENCES `entreprise` (`idEntreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Projet_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `utilisateur_has_projet`
--
ALTER TABLE `utilisateur_has_projet`
  ADD CONSTRAINT `fk_Utilisateur_has_Projet_Utilisateur1` FOREIGN KEY (`Utilisateur_idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Utilisateur_has_Projet_Projet1` FOREIGN KEY (`Projet_idProjet`) REFERENCES `projet` (`idProjet`) ON DELETE NO ACTION ON UPDATE NO ACTION;
